
window.AF ?= {}

pnotify = window.top.PNotify or window.PNotify

PNotify.prototype.options.styling = "fontawesome"

AF.alert = 	
	_backdrop: null

	_shouldWarn: true

	_sticky_backdrop: true

	_current_notification: null

	_body: window.top.$?('body')

	_site_width: AF.utils?.viewport?()?.width

	_default_stack: () ->
		@_stack_config ?= 
			dir1: "down", dir2: "left"
			context: AF.alert._body

	close: () ->
		delete @_current_notification	
		@pnotify_closeAll()

	progress: (type="info", container) ->
		msg = [
			'<div>Processing Request...</div>'
			'<div class="progress progress-striped active" style="margin-top:1em">'
			'<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>'
			'</div>'
		].join ''

		pnotify_config = 
			type: type
			text: msg
			hide: false
			icon: false
			delay: timeout
			container: container
			buttons: closer: false, sticker: false

		AF.alert.pnotify_show pnotify_config

	alert: (msg, type="alert", timeout=3000, container) ->
		msg ?= "Unknown Alert"

		pnotify_config =
			type: type 
			text: msg
			hide: true
			icon: true
			delay: timeout
			container: container
			buttons: closer: false, sticker: false

		AF.alert.pnotify_show pnotify_config

	error: (msg, timeout=4000, container) ->
		msg ?= "Unknown Error"

		pnotify_config = 
			type: "error"
			text: msg
			hide: true
			icon: true
			delay: timeout
			container: container
			buttons: closer: false, sticker: false

		AF.alert.pnotify_show pnotify_config

	stickyError: (msg, container) ->
		msg ?= "Unknown Error"		

		pnotify_config = 
			type: 'error'
			title: 'Errors'
			text: msg
			hide: false
			addclass: "stickyErrorPanel"
			buttons: closer: true, sticker: false
			width: if AF.utils.viewport().width < 400 then width = "300px" else width = "360px"

		$(window).on 'hashchange', (e) => @pnotify_onClose pnotify
		AF.alert.pnotify_show pnotify_config

	warn: (msg, callback, _check_shouldWarn, no_backdrop, container) ->
		AF.alert._shouldWarn = window.top.AF?._shouldWarn unless _.isUndefined window.top.AF?._shouldWarn
		return callback?() unless AF.alert._shouldWarn or _check_shouldWarn is false
		msg ?= "Unknown Warning"
		
		pnotify_onConfirm = (pnotify) => AF.alert.pnotify_onConfirm pnotify, callback	
		pnotify_stopWarnings = (pnotify) => AF.alert.pnotify_stopWarnings pnotify, callback

		pnotify_buttons = [
			{ addClass: "btn btn-af", text: 'Yes', click: pnotify_onConfirm }
			{ addClass: "btn btn-default", text: 'Yes & Stop Warnings', click: pnotify_stopWarnings }
		]

		pnotify_buttons = pnotify_buttons.slice 0, 1 if _check_shouldWarn is false

		pnotify_config = 
			type: 'alert'
			text: msg
			icon: true
			hide: false
			container: container
			sticky_backdrop: false
			backdrop: true unless no_backdrop
			buttons: closer: false, sticker: false
			confirm: confirm: true, buttons: pnotify_buttons

		AF.alert.pnotify_show pnotify_config

	prompt: (msg, callback, dfr, container) ->
		msg ?= "Unknown Prompt"
		msg += '<input type="text" class="noty-prompt-input ui-pnotify-prompt-input" style="width:100%" />'

		pnotify_onClose = (pnotify) => AF.alert.pnotify_onClose pnotify, dfr
		pnotify_onConfirm = (pnotify) => 
			val = AF.alert._body.find('.ui-pnotify .ui-pnotify-prompt-input').val()
			AF.alert.pnotify_onConfirm pnotify, callback, val

		pnotify_buttons = [
			{ addClass: "btn btn-af", text: 'OK', click: pnotify_onConfirm }
			{ addClass: "btn btn-default", text: 'Cancel', click: pnotify_onClose }
		]

		pnotify_config = 
			text: msg
			icon: false
			hide: false
			backdrop: true
			container: container
			sticky_backdrop: false
			buttons: closer: false, sticker: false
			confirm: confirm: true, buttons: pnotify_buttons

		AF.alert.pnotify_show pnotify_config

	confirm: (msg, callback, confirmClass="btn-success", dfr, container, no_backdrop) ->
		AF.alert._shouldWarn = window.top.AF?._shouldWarn unless _.isUndefined window.top.AF?._shouldWarn
		return callback?() unless AF.alert._shouldWarn or dfr is false
		msg ?= "Unknown Confirm"
		pnotify_onClose = (pnotify) => AF.alert.pnotify_onClose pnotify, dfr
		pnotify_onConfirm = (pnotify) => AF.alert.pnotify_onConfirm pnotify, callback
		pnotify_stopWarnings = (pnotify) => AF.alert.pnotify_stopWarnings pnotify, callback
		closeClass = if confirmClass is "btn-danger" then "btn-default" else "btn-danger"
		
		pnotify_buttons = [
			{ addClass: "btn #{closeClass}", text: 'No', click: pnotify_onClose }
			{ addClass: "btn #{confirmClass}", text: 'Yes', click: pnotify_onConfirm }
			{ addClass: "btn btn-default", text: 'Yes & Stop Warnings', click: pnotify_stopWarnings }
		]

		( dfr = undefined; pnotify_buttons = pnotify_buttons.slice 0, 2 ) if dfr is false
		
		pnotify_config = 
			text: msg
			icon: false
			hide: false
			container: container
			sticky_backdrop: true 
			backdrop: true unless no_backdrop
			buttons: closer: false, sticker: false
			confirm: confirm: true, buttons: pnotify_buttons

		AF.alert.pnotify_show pnotify_config

	pnotify_closeAll: ->
		PNotify.removeAll()
		@pnotify_removeBackdrop()

	_removeBackdrop: ->
		@_backdrop?.remove?(); delete @_backdrop 
		@_top_backdrop?.remove?(); delete @_top_backdrop 

	pnotify_removeBackdrop: (force_sticky = false) ->
		unless force_sticky
			return true unless @_sticky_backdrop

		@_backdrop?.addClass('out').removeClass('in')
		@_top_backdrop?.addClass('out').removeClass('in')
		_.delay ( => @_removeBackdrop() ), 1000

	pnotify_onConfirm: (pnotify, callback, val) ->
		@pnotify_removeBackdrop true
		pnotify?.remove?()
		callback? val

	pnotify_onClose: (pnotify, dfr) ->
		@pnotify_removeBackdrop true
		pnotify?.remove?()
		dfr?.reject? false

	pnotify_stopWarnings: (pnotify, callback) ->
		window.top.AF ?= {}
		AF.alert._shouldWarn = false
		window.top.AF._shouldWarn = false
		AF.alert.pnotify_onConfirm pnotify, callback

	pnotify_show: (config = {}) ->

		unless AF.alert._body?.length
			AF.alert._body = window.top.$?('body')

		stack_config = 
			dir1: "down", dir2: "left"
			context: config.container

		defaults =			
			stack: stack_config
			history: history: false
			after_open: (p) -> p?.elem?.find?('input:first').focus()

		config = _.extend config, defaults
		config.stack = @_default_stack() unless stack_config.context

		if config.backdrop and not ( @_backdrop or AF.alert._body.find('.modal-backdrop').length )
			@_backdrop = $('<div class="modal-backdrop fade in"></div>')
			AF.alert._body.append @_top_backdrop = @_backdrop.clone()

		if config.sticky_backdrop then @_sticky_backdrop = true else @_sticky_backdrop = false

		@_current_notification = new PNotify config


